﻿using SarUiWasm.Sar.Helpers;
using SarUiWasm.Sar.Models.Agents;
using SarUiWasm.Sar.Models.Authentication;
using SarUiWasm.Sar.Models.ScriptJobModels;
using SarUiWasm.Sar.Models.ScriptModels;
using SarUiWasm.Sar.Models.Users;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace SarUiWasm.Sar.Services
{
    public class SarApiService
    {
        private readonly HttpClient _httpClient;
        private string _idToken;

        public string IdToken
        {
            get => _idToken;
            set
            {
                _idToken = value;
                AddAuthorizationHeader();
            }
        }

        public SarApiService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private void AddAuthorizationHeader()
        {
            if (!string.IsNullOrEmpty(_idToken))
            {
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _idToken);
            }
        }

        public async Task<ServiceResult<TokenPayload>> GenerateTokens(string user, string password)
        {
            try
            {
                var payload = new TokenRequest()
                {
                    password = password,
                    username = user
                };

                var result = await _httpClient.PostAsJsonAsync("/api/Authentication/login", payload);
                if (result.IsSuccessStatusCode)
                {
                    var tokenPayload = await result.Content.ReadFromJsonAsync<TokenPayload>();
                    return ServiceResult<TokenPayload>.Success(tokenPayload);
                }
                else
                {
                    return ServiceResult<TokenPayload>.Failure("Failed to generate tokens.");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<TokenPayload>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<List<ScriptSimple>>> ListScripts()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<ScriptSimple>>("/api/Script");
                if (response != null)
                {
                    return ServiceResult<List<ScriptSimple>>.Success(response);
                }
                else
                {
                    return ServiceResult<List<ScriptSimple>>.Success(null);
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<List<ScriptSimple>>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<Script>> GetScriptDetails(int scriptId)
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<Script>($"/api/Script/{scriptId}");
                if (response != null)
                {
                    return ServiceResult<Script>.Success(response);
                }
                else
                {
                    return ServiceResult<Script>.Failure("No data returned from the API.");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<Script>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<Script>> CreateScript(ScriptRegisterDto scriptRequest)
        {
            try
            {
                var result = await _httpClient.PostAsJsonAsync("/api/Script/register", scriptRequest);
                if (result.IsSuccessStatusCode)
                {
                    var script = await result.Content.ReadFromJsonAsync<Script>();
                    return ServiceResult<Script>.Success(script);
                }
                else
                {
                    return ServiceResult<Script>.Failure(await result.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<Script>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<Script>> UpdateScript(int scriptId, ScriptUpdateDto updateRequest)
        {
            try
            {
                var result = await _httpClient.PutAsJsonAsync($"/api/Script/{scriptId}", updateRequest);
                if (result.IsSuccessStatusCode)
                {
                    var script = await result.Content.ReadFromJsonAsync<Script>();
                    return ServiceResult<Script>.Success(script);
                }
                else
                {
                    return ServiceResult<Script>.Failure(await result.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<Script>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<bool>> DeleteScript(int scriptId)
        {
            try
            {
                var result = await _httpClient.DeleteAsync($"/api/script/{scriptId}");
                if (result.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                else
                {
                    return ServiceResult<bool>.Failure("Failed to delete script.");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<List<ScriptType>>> ListScriptTypes()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<ScriptType>>($"/api/Script/type");
                if (response != null)
                {
                    return ServiceResult<List<ScriptType>>.Success(response);
                }
                else
                {
                    return ServiceResult<List<ScriptType>>.Success(null);
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<List<ScriptType>>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<bool>> DeleteScriptType(int typeId)
        {
            try
            {
                var result = await _httpClient.DeleteAsync($"/api/Script/type/{typeId}");
                if (result.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                else
                {
                    return ServiceResult<bool>.Failure("Failed to delete script type.");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<ScriptType>> CreateScriptType(ScriptTypeRegisterDto typeRequest)
        {
            try
            {
                var result = await _httpClient.PostAsJsonAsync("/api/Script/type", typeRequest);
                if (result.IsSuccessStatusCode)
                {
                    var scriptType = await result.Content.ReadFromJsonAsync<ScriptType>();
                    return ServiceResult<ScriptType>.Success(scriptType);
                }
                else
                {
                    return ServiceResult<ScriptType>.Failure("Failed to create script type.");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<ScriptType>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<ScriptType>> UpdateScriptType(int typeId, ScriptTypeUpdateDto typeRequest)
        {
            try
            {
                var result = await _httpClient.PutAsJsonAsync($"/api/Script/type/{typeId}", typeRequest);
                if (result.IsSuccessStatusCode)
                {
                    var scriptType = await result.Content.ReadFromJsonAsync<ScriptType>();
                    return ServiceResult<ScriptType>.Success(scriptType);
                }
                else
                {
                    return ServiceResult<ScriptType>.Failure("Failed to update script type.");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<ScriptType>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<List<Agent>>> ListAgents()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<Agent>>("/api/agent");
                if (response != null)
                {
                    return ServiceResult<List<Agent>>.Success(response);
                }
                else
                {
                    return ServiceResult<List<Agent>>.Success(null);
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<List<Agent>>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<AgentRegisterResponseDto>> CreateAgent(AgentRegisterDto request)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync("/api/agent/register", request);
                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadFromJsonAsync<AgentRegisterResponseDto>();
                    return ServiceResult<AgentRegisterResponseDto>.Success(responseData);
                }
                else
                {
                    var errorContent = await response.Content.ReadAsStringAsync();
                    return ServiceResult<AgentRegisterResponseDto>.Failure($"Error: {response.StatusCode}, {errorContent}");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<AgentRegisterResponseDto>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<AgentTokenResetResponseDto>> ResetAgentToken(string agentId)
        {
            try
            {
                var response = await _httpClient.PostAsync($"/api/agent/{agentId}/resettoken", null);
                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadFromJsonAsync<AgentTokenResetResponseDto>();
                    return ServiceResult<AgentTokenResetResponseDto>.Success(responseData);
                }
                else
                {
                    var errorContent = await response.Content.ReadAsStringAsync();
                    return ServiceResult<AgentTokenResetResponseDto>.Failure($"Error: {response.StatusCode}, {errorContent}");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<AgentTokenResetResponseDto>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<bool>> DeleteAgent(string agentId)
        {
            try
            {
                var response = await _httpClient.DeleteAsync($"/api/agent/{agentId}");
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                else
                {
                    var errorContent = await response.Content.ReadAsStringAsync();
                    return ServiceResult<bool>.Failure($"Error: {response.StatusCode}, {errorContent}");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure($"An error occurred: {ex.Message}");
            }
        }

        public async Task<ServiceResult<IEnumerable<AgentCapability>>> ListCapabilities()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<IEnumerable<AgentCapability>>("/api/agentcapability");
                return ServiceResult<IEnumerable<AgentCapability>>.Success(response);
            }
            catch (Exception ex)
            {
                return ServiceResult<IEnumerable<AgentCapability>>.Failure($"Failed to list capabilities: {ex.Message}");
            }
        }

        public async Task<ServiceResult<bool>> AssignCapability(Guid AgentId, Guid CapabilityId)
        {
            try
            {
                var response = await _httpClient.PostAsync($"/api/agent/{AgentId}/capability/{CapabilityId}", null);
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                return ServiceResult<bool>.Failure(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure($"Fatal failure: {ex.Message}");
            }
        }

        public async Task<ServiceResult<bool>> UnassignCapability(Guid AgentId, Guid CapabilityId)
        {
            try
            {
                var response = await _httpClient.DeleteAsync($"/api/agent/{AgentId}/capability/{CapabilityId}");
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                return ServiceResult<bool>.Failure(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure($"Fatal failure: {ex.Message}");
            }
        }

        public async Task<ServiceResult<AgentCapability>> RegisterCapability(AgentCapabilityRegisterDto agentCapabilityRegisterDto)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync("/api/AgentCapability", agentCapabilityRegisterDto);
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<AgentCapability>.Success(await response.Content.ReadFromJsonAsync<AgentCapability>());
                }
                else
                {
                    return ServiceResult<AgentCapability>.Failure($"Failed to register capability : {await response.Content.ReadAsStringAsync()}");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<AgentCapability>.Failure($"Fatal failure: {ex.Message}");
            }
        }

        public async Task<ServiceResult<bool>> DeleteCapability(string CapabilityName)
        {
            try
            {
                var response = await _httpClient.DeleteAsync($"/api/AgentCapability/{CapabilityName}");
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                else
                {
                    return ServiceResult<bool>.Failure($"Failed to delete capability : {await response.Content.ReadAsStringAsync()}");
                }
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure($"Fatal failure: {ex.Message}");
            }
        }

        public async Task<ServiceResult<ScriptJobModel>> ScheduleScriptJob(ScriptJobRequest scriptJobRequest)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync<ScriptJobRequest>($"/api/ScriptJob", scriptJobRequest);
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<ScriptJobModel>.Success(await response.Content.ReadFromJsonAsync<ScriptJobModel>());
                }
                return ServiceResult<ScriptJobModel>.Failure(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                return ServiceResult<ScriptJobModel>.Failure(ex.Message);
            }
        }

        public async Task<ServiceResult<List<ScriptJobModel>>> ListRecentJobs()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<ScriptJobModel>>("/api/ScriptJob/list");
                return ServiceResult<List<ScriptJobModel>>.Success(response);
            }
            catch (Exception ex)
            {
                return ServiceResult<List<ScriptJobModel>>.Failure(ex.Message);
            }
        }

        #region users

        public async Task<ServiceResult<List<UserModel>>> ListUsers()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<UserModel>>("/api/users");
                return ServiceResult<List<UserModel>>.Success(response);
            }
            catch (Exception ex)
            {
                return ServiceResult<List<UserModel>>.Failure(ex.Message);
            }
        }

        public async Task<ServiceResult<UserModel>> ApproveUser(string UserName)
        {
            try
            {
                var response = await _httpClient.PostAsync($"/api/users/{UserName}/approve", null);
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<UserModel>.Success(await response.Content.ReadFromJsonAsync<UserModel>());
                }
                return ServiceResult<UserModel>.Failure(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                return ServiceResult<UserModel>.Failure(ex.Message);
            }
        }

        public async Task<ServiceResult<UserModel>> CreateUser(UserRegisterDto userRegisterDto)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync("/api/users/register", userRegisterDto);
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<UserModel>.Success(await response.Content.ReadFromJsonAsync<UserModel>());
                }
                return ServiceResult<UserModel>.Failure(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                return ServiceResult<UserModel>.Failure(ex.Message);
            }
        }

        public async Task<ServiceResult<bool>> DeleteUser(string UserName)
        {
            try
            {
                var response = await _httpClient.DeleteAsync($"/api/users/{UserName}");
                if (response.IsSuccessStatusCode)
                {
                    return ServiceResult<bool>.Success(true);
                }
                return ServiceResult<bool>.Failure(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                return ServiceResult<bool>.Failure(ex.Message);
            }
        }

        #endregion users
    }
}