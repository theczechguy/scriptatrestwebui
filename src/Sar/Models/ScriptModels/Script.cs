﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class Script
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public ScriptType ScriptType { get; set; }
        public int Timeout { get; set; }
        public DateTime LastModifiedUtc { get; set; }
        public List<ScriptHistory> ScriptHistory { get; set; }

        public ScriptUpdateDto ToUpdateDto()
        {
            return new ScriptUpdateDto
            {
                EncodedContent = EncodedContent,
                ScriptType = ScriptType,
                Timeout = Timeout,
                Name = Name
            };
        }
    }
}