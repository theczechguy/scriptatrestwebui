﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class ScriptRegisterDto
    {
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public int type { get; set; }
        public int Timeout { get; set; }
    }
}