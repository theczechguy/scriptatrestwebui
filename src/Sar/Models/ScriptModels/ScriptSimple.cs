﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class ScriptSimple
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}