﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class ScriptTypeUpdateDto
    {
        public string Name { get; set; }
        public string Runner { get; set; }
        public string FileExtension { get; set; }
        public string ScriptArgument { get; set; }
    }
}