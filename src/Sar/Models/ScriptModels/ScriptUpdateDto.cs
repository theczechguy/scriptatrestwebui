﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class ScriptUpdateDto
    {
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public ScriptType ScriptType { get; set; }
        public int Timeout { get; set; }
    }
}