﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class ScriptType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Runner { get; set; }
        public string FileExtension { get; set; }
        public string ScriptArgument { get; set; }

        public ScriptTypeUpdateDto ToUpdateDto()
        {
            return new ScriptTypeUpdateDto
            {
                FileExtension = FileExtension,
                ScriptArgument = ScriptArgument,
                Name = Name,
                Runner = Runner
            };
        }
    }
}