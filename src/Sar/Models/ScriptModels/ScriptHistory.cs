﻿namespace SarUiWasm.Sar.Models.ScriptModels
{
    public class ScriptHistory
    {
        public int Id { get; set; }
        public int ScriptEntityId { get; set; }
        public string Name { get; set; }
        public string EncodedContent { get; set; }
        public ScriptType ScriptType { get; set; }
        public int Timeout { get; set; }
        public DateTime ModifiedUtc { get; set; }
    }
}