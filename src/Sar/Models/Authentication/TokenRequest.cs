﻿namespace SarUiWasm.Sar.Models.Authentication
{
    public class TokenRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}