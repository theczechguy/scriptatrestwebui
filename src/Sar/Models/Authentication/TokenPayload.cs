﻿namespace SarUiWasm.Sar.Models.Authentication
{
    public class TokenPayload
    {
        public string Token { get; set; }
        public DateTime TokenExpiration { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpiration { get; set; }
    }
}