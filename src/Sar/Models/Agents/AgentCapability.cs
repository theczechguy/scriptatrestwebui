﻿namespace SarUiWasm.Sar.Models.Agents
{
    public class AgentCapability
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}