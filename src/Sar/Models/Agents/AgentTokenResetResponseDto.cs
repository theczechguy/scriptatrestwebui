﻿namespace SarUiWasm.Sar.Models.Agents
{
    public class AgentTokenResetResponseDto
    {
        public string token { get; set; }
    }
}