﻿namespace SarUiWasm.Sar.Models.Agents
{
    public class AgentCapabilityRegisterDto
    {
        public string name { get; set; }
        public string description { get; set; }
    }
}