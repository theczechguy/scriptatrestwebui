﻿namespace SarUiWasm.Sar.Models.Agents
{
    public class Agent
    {
        public Guid Id { get; set; }
        public string UserDefinedName { get; set; }
        public string ConnectionStatus { get; set; }
        public DateTime LastConnected { get; set; }
        public string ConnectionId { get; set; }
        public List<Guid> AgentCapabilityIds { get; set; }
    }
}