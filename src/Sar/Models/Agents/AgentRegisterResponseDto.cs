﻿namespace SarUiWasm.Sar.Models.Agents
{
    public class AgentRegisterResponseDto
    {
        public string agentName { get; set; }
        public string agentId { get; set; }
        public string token { get; set; }
    }
}

//{
//    "agentName": "jednicka",
//	"agentId": "456aa697-a926-4adc-b27e-e1da48130636",
//	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWdlbnRfamVkbmlja2EiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjQ4NTYzMTA4LWYwMzItNDVmNC1iZTc1LTMyMTUwYjQ3ZjE4MyIsIm5iZiI6MTY5NTI5Nzk4NiwiZXhwIjoxNjk1MzEyMzg2LCJpc3MiOiJodHRwOi8vMTI3LjAuMC4xOjUwMDAiLCJhdWQiOiJodHRwOi8vMTI3LjAuMC4xOjUwMDAifQ.Q_yG2auLmgEbuMFeJf9_wiaTKtV7cvZqP-cNW9Y5C-k"
//}