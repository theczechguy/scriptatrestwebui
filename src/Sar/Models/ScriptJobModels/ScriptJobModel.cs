﻿namespace SarUiWasm.Sar.Models.ScriptJobModels
{
    public class ScriptJobModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string JobStatus { get; set; }
        public string EncodedJobStandardOutput { get; set; }
        public string EncodedJobErrorOutput { get; set; }
        public int JobExitCode { get; set; }
        public string FailureDetails { get; set; }
        public Guid AssignedAgentId { get; set; }
        public int ScriptId { get; set; }
    }
}