﻿namespace SarUiWasm.Sar.Models.ScriptJobModels
{
    public class ScriptJobRequest
    {
        public List<ScriptParamModel> ScriptParamModel { get; set; }
        public string RequiredCapabilityName { get; set; }
        public int ScriptId { get; set; }
    }
}