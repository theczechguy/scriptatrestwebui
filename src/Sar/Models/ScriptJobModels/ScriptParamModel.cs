﻿namespace SarUiWasm.Sar.Models.ScriptJobModels
{
    public class ScriptParamModel
    {
        public string Name { get; set; }
        public string EncodedValue { get; set; }
    }
}