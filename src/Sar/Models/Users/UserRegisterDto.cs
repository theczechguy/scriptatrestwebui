﻿namespace SarUiWasm.Sar.Models.Users
{
    public class UserRegisterDto
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}