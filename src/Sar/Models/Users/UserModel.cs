﻿namespace SarUiWasm.Sar.Models.Users
{
    public class UserModel
    {
        public Guid id { get; set; }
        public string username { get; set; }
        public bool approved { get; set; }
    }
}