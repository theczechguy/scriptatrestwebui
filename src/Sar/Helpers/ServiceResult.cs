﻿namespace SarUiWasm.Sar.Helpers
{
    /// <summary>
    /// Encapsulates the result of a service call, indicating whether it was successful or not.
    /// </summary>
    /// <typeparam name="T">The type of the value returned on success.</typeparam>
    public class ServiceResult<T>
    {
        /// <summary>
        /// Gets a value indicating whether the service call was successful.
        /// </summary>
        public bool IsSuccess { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the service call failed.
        /// </summary>
        public bool IsFailure => !IsSuccess;

        /// <summary>
        /// Gets the value returned by a successful service call.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Gets the error message returned by a failed service call.
        /// </summary>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceResult{T}"/> class.
        /// </summary>
        /// <param name="isSuccess">Indicates whether the service call was successful.</param>
        /// <param name="value">The value returned by the service call, if successful.</param>
        /// <param name="errorMessage">The error message returned by the service call, if failed.</param>
        private ServiceResult(bool isSuccess, T value, string errorMessage)
        {
            IsSuccess = isSuccess;
            Value = value;
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Creates a new instance of <see cref="ServiceResult{T}"/> representing a successful service call.
        /// </summary>
        /// <param name="value">The value returned by the successful service call.</param>
        /// <returns>A new instance of <see cref="ServiceResult{T}"/>.</returns>
        public static ServiceResult<T> Success(T value)
        {
            return new ServiceResult<T>(true, value, null);
        }

        /// <summary>
        /// Creates a new instance of <see cref="ServiceResult{T}"/> representing a failed service call.
        /// </summary>
        /// <param name="errorMessage">The error message returned by the failed service call.</param>
        /// <returns>A new instance of <see cref="ServiceResult{T}"/>.</returns>
        public static ServiceResult<T> Failure(string errorMessage)
        {
            return new ServiceResult<T>(false, default(T), errorMessage);
        }
    }
}