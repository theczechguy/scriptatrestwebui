﻿using System.Text;

namespace SarUiWasm.Sar.Helpers
{
    public static class CommonHelpers
    {
        public static string DecodeBase64(string base64String)
        {
            byte[] bytes = Convert.FromBase64String(base64String);
            return Encoding.UTF8.GetString(bytes);
        }

        public static string EncodeBase64(string content)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(content);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static int CalculateTextareaRows(string content)
        {
            if (string.IsNullOrEmpty(content)) return 10;
            return content.Split('\n').Length;
        }
    }
}