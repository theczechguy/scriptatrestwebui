﻿@page "/types"
@using SarUiWasm.Auth
@using SarUiWasm.Components
@using SarUiWasm.Sar.Models.Authentication
@using SarUiWasm.Sar.Models.ScriptModels
@using SarUiWasm.Sar.Services
@using Microsoft.AspNetCore.Components.QuickGrid

@inject HttpClient Http
@inject IJSRuntime JS

<PageTitle>Script Types</PageTitle>
<AuthorizeView Context="authorizeviewContext">
    <NotAuthorized>
        <div>This page can be accessed only after successful authentication</div>
    </NotAuthorized>
    <Authorized Context="authorizedContext">
        <h1>Script Types</h1>
        <div class="alert alert-info">
            <ul>
                <li>By defining a Type, you configure how to execute a specific script or executable.</li>
                <li>Please ensure that the executable specified in the Type is available on the system hosting your agents.</li>
                <li>Note that it is not possible to delete a Type if it is being used by any Script.</li>
                <li><strong>Example:</strong></li>
                <ul>
                    <li><strong>Name:</strong> <em>Powershell7</em> - A descriptive name for the script type.</li>
                    <li><strong>Runner:</strong> <em>pwsh.exe</em> - The executable used to run the script.</li>
                    <li><strong>File Extension:</strong> <em>.ps1</em> - The file extension associated with the script type.</li>
                    <li><strong>Script Argument:</strong> <em>-f</em> - The argument to be passed to the script executable.</li>
                </ul>
            </ul>
        </div>

        <ul>
            <li>Please be aware it is not possible to delete types that have existing scripts using them</li>
        </ul>
        @if (types == null)
        {
            <div class="d-flex justify-content-center my-5">
                <div class="spinner-border text-primary" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </div>
        }
        else
        {
            <button class="btn btn-primary mb-3" @onclick="ShowNewTypeForm">New Type</button>
            <div class="table-responsive">
                <QuickGrid Items="@types" Class="table table-striped table-hover">
                    <PropertyColumn Align="Align.Center" Property="@(type => type.Id)" Title="ID" Class="text-center" />
                    <PropertyColumn Align="Align.Center" Property="@(type => type.Name)" Title="Name" Class="text-center" />
                    <PropertyColumn Align="Align.Center" Property="@(type => type.Runner)" Title="Runner" Class="text-center" />
                    <PropertyColumn Align="Align.Center" Property="@(type => type.FileExtension)" Title="File Extension" Class="text-center" />
                    <PropertyColumn Align="Align.Center" Property="@(type => type.ScriptArgument)" Title="Script Argument" Class="text-center" />
                    <TemplateColumn Align="Align.Center" Title="Actions" Class="align-content-center text-center">
                        <div class="btn-group d-flex justify-content-center">
                            <button class="btn btn-primary btn-sm me-1" style="max-width: 100px;" @onclick="() => EditType(context)">Edit</button>
                            <button class="btn btn-danger btn-sm" style="max-width: 100px;" @onclick="() => DeleteType(context)">Delete</button>
                        </div>
                    </TemplateColumn>
                </QuickGrid>
            </div>
        }
        @if (showNewTypeForm)
        {
            <div class="card mt-3" id="create-script-form">
                <div class="card-body">
                    <h2 class="card-title">New script type</h2>
                    <EditForm Model="newType" OnValidSubmit="CreateType" Context="newTypeEditFormContext">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <InputText class="form-control" id="name" @bind-Value="newType.Name" />
                        </div>
                        <div class="form-group">
                            <label for="runner">Runner:</label>
                            <InputText class="form-control" id="runner" @bind-Value="newType.Runner" />
                        </div>
                        <div class="form-group">
                            <label for="fileExtension">File Extension:</label>
                            <InputText class="form-control" id="fileExtension" @bind-Value="newType.FileExtension" />
                        </div>
                        <div class="form-group">
                            <label for="scriptArgument">Script Argument:</label>
                            <InputText class="form-control" id="scriptArgument" @bind-Value="newType.ScriptArgument" />
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success w-100">Create</button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-secondary w-100" @onclick="HideNewTypeForm">Close</button>
                            </div>
                        </div>
                    </EditForm>
                </div>
            </div>
        }

        @if (selectedTypeForEdit != null && isEditModalVisible == true)
        {
            <EditTypeModal IsVisible="isEditModalVisible" TypeToEdit="selectedTypeForEdit" OnSave="HandleSaveChanges" OnClose="CloseEditTypeModal" />
        }

    </Authorized>
</AuthorizeView>

@code {
    private bool showNewTypeForm = false;

    private TokenPayload tokenPayload;
    private SarApiService sarApi;
    private IQueryable<ScriptType> types;
    private ScriptTypeRegisterDto newType = new ScriptTypeRegisterDto();

    private bool isEditModalVisible = false;

    private ScriptType selectedTypeForEdit;

    private void EditType(ScriptType type)
    {
        selectedTypeForEdit = type;
        isEditModalVisible = true;
        showNewTypeForm = false; // Ensure new type form is hidden
    }

    private async Task HandleSaveChanges(ScriptType updatedType)
    {
        await UpdateType(updatedType.Id, updatedType);
        await ListScriptTypes();
        StateHasChanged();
        await CloseEditTypeModal();
    }

    private async Task CloseEditTypeModal()
    {
        isEditModalVisible = false;
        selectedTypeForEdit = null;
    }

    protected override async Task OnInitializedAsync()
    {
        sarApi = new SarApiService(Http);
        var tokenGenerator = new MsalTokens(JS);
        sarApi.IdToken = await tokenGenerator.GetJwtToken();

        await ListScriptTypes();
    }
    private void ClearNewType()
    {
        newType = null;
    }
    private void HideNewTypeForm()
    {
        showNewTypeForm = false;
    }

    private async void ShowNewTypeForm()
    {
        showNewTypeForm = true;
        isEditModalVisible = false; // Ensure edit modal is hidden
        selectedTypeForEdit = null; // Reset the selected type for edit

        StateHasChanged();
        await Task.Delay(100);
        await JS.InvokeVoidAsync("scrollToForm", "create-script-form");
    }

    private async void DeleteType(ScriptType type)
    {
        if (sarApi.IdToken is not null)
        {
            if (await JS.InvokeAsync<bool>("confirmDelete", "Are you sure you want to delete this type?"))
            {
                var result = await sarApi.DeleteScriptType(type.Id);
                if (result.IsSuccess)
                {
                    await JS.InvokeVoidAsync("showToast", $"Script type: {type.Id} deleted successfully!", "success");
                }
                else
                {
                    await JS.InvokeVoidAsync("showToast", "Failed to delete script type.", "error");
                }
                await ListScriptTypes();
            }
        }
    }

    private async Task ListScriptTypes()
    {
        var result = await sarApi.ListScriptTypes();
        if (result.IsFailure)
        {
            await JS.InvokeVoidAsync("showToast", "Failed to list types", "error");
        }
        if (result.IsSuccess && result.Value is null)
        {
            await JS.InvokeVoidAsync("showToast", "No types found", "warning");
        }
        if (result.IsSuccess)
        {
            types = result.Value.AsQueryable();
            StateHasChanged();
        }        
    }

    private async Task CreateType()
    {
        if (sarApi.IdToken is not null)
        {
            var result = await sarApi.CreateScriptType(newType);
            if (result.IsSuccess)
            {
                await JS.InvokeVoidAsync("showToast", $"Type: {result.Value.Id} created successfully!", "success");
                await ListScriptTypes();
                showNewTypeForm = false; // Hide the form after creation
            }
            else
            {
                await JS.InvokeVoidAsync("showToast", "Failed to create type.", "error");
            }
        }
    }

    private async Task UpdateType(int typeId, ScriptType scriptType)
    {
        if (sarApi.IdToken is not null)
        {
            var result = await sarApi.UpdateScriptType(typeId, scriptType.ToUpdateDto());
            if (result.IsSuccess)
            {
                await JS.InvokeVoidAsync("showToast", $"Type: {result.Value.Id} updated successfully!", "success");
            }
            else
            {
                await JS.InvokeVoidAsync("showToast", "Failed to update type.", "error");
            }
        }
    }
}
