﻿using Microsoft.JSInterop;

namespace SarUiWasm.Auth
{
    public class MsalTokens
    {
        private readonly IJSRuntime _jsRuntime;

        public MsalTokens(IJSRuntime jSRuntime)
        {
            _jsRuntime = jSRuntime;
        }

        public async Task<string> GetJwtToken()
        {
            string token = null;

            // Uses MSAL when working with AAD only
            token = await this._jsRuntime
            .InvokeAsync<string>("getMsalToken", "hello");
            return token;
        }
    }
}