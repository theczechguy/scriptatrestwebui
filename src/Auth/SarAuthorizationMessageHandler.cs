﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SarUiWasm.Auth
{
    public class SarAuthorizationMessageHandler : AuthorizationMessageHandler
    {
        public SarAuthorizationMessageHandler(IAccessTokenProvider provider,
    NavigationManager navigation)
    : base(provider, navigation)
        {
            ConfigureHandler(
                authorizedUrls: new[] { "http://127.0.0.1:5000" });
        }
    }
}