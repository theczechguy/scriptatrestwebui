﻿using Microsoft.JSInterop;

namespace SarUiWasm.Helpers
{
    public interface INotificationHelper
    {
        public Task NotifySuccess(string message);

        public Task NotifyWarning(string message);

        public Task NotifyError(string message);
    }

    public class NotificationHelper : INotificationHelper
    {
        private IJSRuntime iJS;

        public NotificationHelper(IJSRuntime iJS)
        {
            this.iJS = iJS;
        }

        public async Task NotifySuccess(string message)
        {
            await iJS.InvokeVoidAsync("showToast", message, "success");
        }

        public async Task NotifyError(string message)
        {
            await iJS.InvokeVoidAsync("showToast", message, "error");
        }

        public async Task NotifyWarning(string message)
        {
            await iJS.InvokeVoidAsync("showToast", message, "warning");
        }
    }
}