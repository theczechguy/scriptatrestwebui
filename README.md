# ScriptAtRestWebUi
- This is a frontend for a [SAR](https://gitlab.com/theczechguy/ScriptAtRest) project
- The UI covers the most commonly used actions, but does provide the full coverage of all of the SAR api endpoints
- Authentication is performed through Microsoft EntraId

## Setup
### Entra ID
- create a new ```App registration```
 - use any name you want
 - keep the default supported account types
 - in the Redirect URI section
   - for platform choose ***Single-page application(SPA)***
   - for the url put ***https://sarui:5000/authentication/login-callback***
     - instead of ***sarui:5000*** use the uri that points to your SAR WEB UI installation
- mark down following details
  - Client ID
  - Tenant ID
- go to ```Authentication``` section
- in the ***Implicit grant and hybrid flows*** check both ***Access tokens*** and ***ID tokens***
- go to ```Expose an API``` section
- next to ***Application ID URI*** click on ***Add***
- keep the pre filled value and hit ***Save***
- click on ***Add a scope***
  - for all mandatory fields put as value ***default***
  - after scope is created copy the full scope path
    - starting with ```api://```
- go to ```App roles``` section
- Create a new app roles with following names, set the value to be the same
  - ***Approved***
  - ***Admin***
  - in both cases set the Allowed member type to ***Users/Groups***
- go back to ```Overview``` section
- click on ***Managed application in local directory***
- go to ```Users and groups``` section
- assign the desired users or groups the appropriate role
  - ***Approved*** role allows users to perform limited ammount of actions
  - Admin role gives unlimited access to the SAR platform
  - All users must have ***Approved*** role and optionally ***Admin*** role if needed
  - Users without ***Approved*** role are not authorized to perform any actions at all


  ### Web UI
  - SAR Web UI is using WASM technology, this means the website is hosted as an static website without any active components
  therefore does not require a lot of compute power to operate
  #### IIS setup
  - download web site package [SARUI](https://sarreleases.blob.core.windows.net/releases/SarUi.zip)
  - Create a new site in IIS manager
  - Unpack the package to a folder you specified when creating new site
  - open the ```Application Pools``` in IIS manager
  - open the pool created for your new site
  - for the ***.NET CLR version:*** choose ```No Managed Code```

  - open the ```wwwroot``` folder of SAR UI
  - open the ```appsettings.json``` file
  - in the ***AzureAd*** section
    - in the Authority put ```https://login.microsoftonline.com/tenantid```
    - in the ClientId put your app registration ClientID
    - in the Scopes put your app registration full scope path
      - ```api://xxxxxxxxxx-xxxxxx/default```
    - Note: these values are not of a sensitive nature
  - In the ***SarApi*** section
    - in the BaseUrl set the URL of your SAR API installation
      - example: ```https://127.0.0.1:5050"```
  - start the site